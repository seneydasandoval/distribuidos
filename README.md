﻿Respositorio Gitlab:
https://gitlab.com/seneydasandoval/distribuidos.git

Estructura de la Base de Datos:
Secuencias

CREATE SEQUENCE public.id_asignatura_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 999999999
  START 8
  CACHE 1;
ALTER TABLE public.id_asignatura_seq
  OWNER TO postgres;

CREATE SEQUENCE public.id_asociacion_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 999999999
  START 8
  CACHE 1;
ALTER TABLE public.id_asociacion_seq
  OWNER TO postgres;

Tablas:
CREATE TABLE public.asignatura
(
  id integer NOT NULL DEFAULT nextval('id_asignatura_seq'::regclass),
  nombre character varying(100),
  CONSTRAINT pk_id_asignatura PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.asignatura
  OWNER TO postgres;

CREATE TABLE public.asociacion
(
  id integer NOT NULL DEFAULT nextval('id_asociacion_seq'::regclass),
  persona integer NOT NULL,
  asignatura integer NOT NULL,
  CONSTRAINT pk_asociacion PRIMARY KEY (id),
  CONSTRAINT fk_asignatura FOREIGN KEY (asignatura)
      REFERENCES public.asignatura (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_persona FOREIGN KEY (persona)
      REFERENCES public.persona (cedula) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT uk_asociacion UNIQUE (persona, asignatura)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.asociacion
  OWNER TO postgres;

CREATE TABLE public.persona
(
  cedula integer NOT NULL,
  nombre character varying(1000),
  apellido character varying(1000),
  CONSTRAINT pk_cedula PRIMARY KEY (cedula)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.persona
  OWNER TO postgres;



Verificación con herramienta SOAP-UI: 
GET http://localhost:8080/personas/rest/asignatura
POST http://localhost:8080/personas/rest/asignatura {"nombtre":"ecologia"}
POST http://localhost:8080/personas/rest/asignatura/modificar {"id":1,"nombtre":"ecologia"}
DELETE://localhost:8080/personas/rest/asignatura/2

POST http://localhost:8080/personas/rest/asociacion/crear?persona=123&asignatura=6
DELETE http://localhost:8080/personas/rest/asociacion/borrar?persona=123&asignatura=6
GET http://localhost:8080/personas/rest/asignaturas/123
GET http://localhost:8080/personas/rest/personas/7

