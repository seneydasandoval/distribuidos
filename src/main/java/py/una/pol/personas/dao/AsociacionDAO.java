package py.una.pol.personas.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import py.una.pol.personas.model.*;

@Stateless
public class AsociacionDAO {
 
	
    @Inject
    private Logger log;
    
	/**
	 * 
	 * @param condiciones 
	 * @return
	 */

    public List<Asignatura> seleccionarAsignaturas(long cedula) {
		String query = "SELECT ag.id, ag.nombre FROM asociacion a " + 
						"LEFT JOIN asignatura ag on a.asignatura = ag.id " + 
						"where a.persona = ? ";
		
		List<Asignatura> lista = new ArrayList<Asignatura>();
		
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(query);
        	pstmt.setLong(1, cedula);
        	
        	ResultSet rs = pstmt.executeQuery();
        	while(rs.next()) {
        		Asignatura p = new Asignatura();
        		p.setId(rs.getLong(1));
        		p.setNombre(rs.getString(2));
        		lista.add(p);
        	}
        	
        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

	}
	
	public List<Persona> seleccionarPersonas(long id) {
		String query = "SELECT p.cedula, p.nombre, p.apellido FROM asociacion a " + 
						"LEFT JOIN persona p on a.persona = p.cedula " + 
						"where a.asignatura = ? ";
		
		List<Persona> lista = new ArrayList<Persona>();
		
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(query);
        	pstmt.setLong(1, id);
        	
        	ResultSet rs = pstmt.executeQuery();
        	while(rs.next()) {
        		Persona p = new Persona();
        		p.setCedula(rs.getLong(1));
        		p.setNombre(rs.getString(2));
        		p.setApellido(rs.getString(3));
        		lista.add(p);
        	}
        	
        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

	}

	public long crearAsociacion(Asociacion p) throws SQLException {

        String SQL = "INSERT INTO asociacion(persona,asignatura) "
                + "VALUES(?,?)";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setLong(1, p.getPersona());
            pstmt.setLong(2, p.getAsignatura());
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        	
        return id;
    	
    	
    }

    
    public long borrarAsociacion(Asociacion p) throws SQLException {
        String SQL = "DELETE FROM asociacion WHERE persona = ? and asignatura = ?";
         Connection conn = null;
        long id = 0;
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setLong(1, p.getPersona());
            pstmt.setLong(2, p.getAsignatura());
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	log.severe("Error en la eliminación: " + ex.getMessage());
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	log.severe("Error en la eliminación: " + ex.getMessage());
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        		throw ef;
        	}
        }
        return id;
    }
    

}
