/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.una.pol.personas.service;


import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.dao.*;
import py.una.pol.personas.model.*;

import java.util.List;
import java.util.logging.Logger;

// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class AsociacionService {

    @Inject
    private Logger log;

    @Inject
    private AsociacionDAO dao;

    public void crear(Asociacion p) throws Exception {
        log.info("Creando Asociacion: Persona " + p.getPersona() + " Asignatura " + p.getAsignatura() );
        try {
        	dao.crearAsociacion(p);
        }catch(Exception e) {
        	log.severe("ERROR al crear Asociacion: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Asociacion creada con éxito: Persona " + p.getPersona() + " Asignatura " + p.getAsignatura() );
    }


    public void eliminar(Asociacion p) throws Exception {
    	log.info("Eliminando Asociacion con éxito: Persona " + p.getPersona() + " Asignatura " + p.getAsignatura() );
        try {
        	dao.borrarAsociacion(p);
        }catch(Exception e) {
        	log.severe("ERROR al eliminar Asociacion: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Asociacion eliminada con éxito: Persona " + p.getPersona() + " Asignatura " + p.getAsignatura() );
    }


    public List<Persona> alumnos(long id) {
    	return dao.seleccionarPersonas(id);
    }
    
    public List<Asignatura> asignaturas(long cedula) {
    	return dao.seleccionarAsignaturas(cedula);
    }

}
