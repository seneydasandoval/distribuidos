package py.una.pol.personas.model;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;


@SuppressWarnings("serial")
@XmlRootElement
public class Asociacion  implements Serializable {
	long id;
	long persona;
	long asignatura;

	
	public Asociacion() {
	}

	public Asociacion(Long id, Long persona, Long asignatura) {
		this.id = id;
		this.persona = persona;
		this.asignatura = asignatura;	
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getPersona() {
		return persona;
	}

	public void setPersona(long persona) {
		this.persona = persona;
	}

	public long getAsignatura() {
		return asignatura;
	}

	public void setAsignatura(long asignatura) {
		this.asignatura = asignatura;
	}
	

}
