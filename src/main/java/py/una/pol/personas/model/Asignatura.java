package py.una.pol.personas.model;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;


@SuppressWarnings("serial")
@XmlRootElement
public class Asignatura  implements Serializable {

	Long id;
	String nombre;

	
	public Asignatura(){
	}

	public Asignatura(Long pid, String pnombre){
		this.id = pid;
		this.nombre = pnombre;	
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
